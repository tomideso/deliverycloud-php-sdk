# DeliveryCloud SDK for PHP

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

** ```Kolawole``` ```bamtak``` ```shoppi.ng``` ```kolawolet@shoppi.ng``` ```shoppi``` ```deliverycloud-sdk-php``` ```DeliveryCloud SDK for PHP - Use Shopping Deivery Cloud Services in your PHP project``` 

The Delivery Cloud SDK for PHP makes it easy for developers to access Shopp!ng Delivery Cloud Services 
in their PHP code to build application. You can get started in minutes by installing the SDK through Composer



## Install

Via Composer

``` bash
$ composer require shoppi/deliverycloud-sdk-php
```

## Usage

``` php
$deliveryCloud = new Shoppi\DeliveryCloud();
echo $deliveryCloud >createShipment($shipment);
```

## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email kolawolet@shoppi.ng instead of using the issue tracker.

## Credits

- [Kolawole][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/shoppi/deliverycloud-sdk-php.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/shoppi/deliverycloud-sdk-php/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/shoppi/deliverycloud-sdk-php.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/shoppi/deliverycloud-sdk-php.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/shoppi/deliverycloud-sdk-php.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/shoppi/deliverycloud-sdk-php
[link-travis]: https://travis-ci.org/shoppi/deliverycloud-sdk-php
[link-scrutinizer]: https://scrutinizer-ci.com/g/shoppi/deliverycloud-sdk-php/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/shoppi/deliverycloud-sdk-php
[link-downloads]: https://packagist.org/packages/shoppi/deliverycloud-sdk-php
[link-author]: https://github.com/bamtak
[link-contributors]: ../../contributors
