<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class holds Shipment information
 *
 * @author bamtak
 */
class Shipment {

    /**  @var string item_cost */
    public $item_cost;
    
    /**  @var string merchant_id */
    public $merchant_id;
    
    /**  @var customer customer */
    public $customer_object;
    
    /**  @var seller seller */
    public $seller_object;
    
    /**  @var string delivery_provider */
    public $delivery_provider;
    
    /**  @var string delivery_service */
    public $delivery_service;
    
    /**  @var string payment_mode */
    public $payment_mode;
    
    /**  @var string verification_mode */
    public $verification_mode;
    
    /**  @var string products_array */
    public $products_array;

    public function __construct($item_cost, $merchant_id, Customer $customer, Seller $seller, $delivery_provider, $delivery_service, $payment_mode, $verification_mode, $products_array) {
        $this->item_cost = $item_cost;
        $this->merchant_id = $merchant_id;
        $this->customer_object = $customer;
        $this->seller_object = $seller;
        $this->delivery_provider = $delivery_provider;
        $this->delivery_service = $delivery_service;
        $this->payment_mode = $payment_mode;
        $this->verification_mode = $verification_mode;
        $this->products_array = $products_array;
    }
}

