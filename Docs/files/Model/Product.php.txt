<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class for product information
 * @author bamtak
 */
class Product {

    /**  @var string product ID */
    public $product_id;
    
    /**  @var string product description */
    public $description;
    
    /**  @var string summary */
    public $summary;
    
    /**  @var string quantity_in_bike*/
    public $quantity_in_bike;
    
    /**  @var string quantities */
    public $quantity;
    
    /**  @var string weight */
    public $weight;

    public function __construct($product_id, $description, $summary, $quantity_in_bike, $quantity, $weight) {
        $this->product_id = $product_id;
        $this->description = $description;
        $this->summary = $summary;
        $this->quantity_in_bike = $quantity_in_bike;
        $this->quantity = $quantity;
        $this->weight = $weight;
    }
}

