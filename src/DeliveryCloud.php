<?php
namespace Shoppi;

use Shoppi\RequestHandler;
use Shoppi\model\Shipment;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * To start working with DeliveryCloud you need to register a developer account
 * with Shopping and and obtained your $client_id, $client_secret
 * @author bamtak
 */
class DeliveryCloud implements DeliveryCloudInterface {

    /** @var string */
    private $requestHandler;

    /**
     * @param String $client_id in your developer account
     * @param String $client_secret in your developer account
     */
    public function __construct($client_id, $client_secret) {
        $this->requestHandler = new RequestHandler($client_id, $client_secret);
    }

    /**
     * Help to create new shipment
     * @param Shipment $shipment object that contained shipment information
     * @return Array of response body from server
     */
    public function createShipment(Shipment $shipment) {
        return $this->requestHandler->sendPostRequest("shipments", $shipment);
    }

    /**
     * Get all available verification methods associated with your account
     * @return Array of response body from server
     */
    public function getCustomerVerificationMethods() {
        return $this->requestHandler->sendGetRequest("verifications");
    }

    /**
     * Get available delivery categories
     * @return Array of response body from server
     */
    public function getDeliveryCategories() {
        return $this->requestHandler->sendGetRequest("delivery_categories");
    }

    /**
     * Get available delivery services
     * @return Array of response body from server
     */
    public function getDeliveryServices() {
        return $this->requestHandler->sendGetRequest("delivery_services");
    }

    /**
     * Get available payment modes
     * @return Array of response body from server
     */
    public function getPaymentMode() {
        return $this->requestHandler->sendGetRequest("payment_modes");
    }

    /**
     * Get shipment options for an order
     * @param array $arguments [town_id,destination_town_id,delivery_service_id,
     * category_id,cost,payment_mode_id]
     * @return array of response body from server
     */
    public function getShipmentOptions(array $arguments) {
        $endpoint = "shipment_options/town/" . $arguments->town_id .
                "/destination/" . $arguments->destination_town_id .
                "/delivery_service/" . $arguments->delivery_service_id .
                "/category/" . $arguments->category_id .
                "/cost/" . $arguments->cost .
                "/payment_mode/" . $arguments->payment_mode_id;
        return $this->requestHandler->sendGetRequest($endpoint);
    }

    /**
     * Get all available shipment statuse
     * @return Array of response body from server
     */
    public function getShipmentStatuses() {
        return $this->requestHandler->sendGetRequest("shipment_status");
    }

    /*
     * Get all available town IDs
     * @return Array of response body from server
     */

    public function getTownIDs() {
        return $this->requestHandler->sendGetRequest("location/towns");
    }

}
