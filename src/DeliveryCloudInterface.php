<?php
namespace Shoppi;

use Shoppi\model\Shipment;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author bamtak
 */
interface DeliveryCloudInterface {
    /*
     * 
     * $town_id,$destination_town_id,$delivery_service_id,$category_id,$payment_mode_id
     * and open the template in the editor.
     */

    public function getShipmentOptions(array $arguments);

    public function createShipment(Shipment $shipment);

    public function getDeliveryServices();

    public function getDeliveryCategories();

    public function getPaymentMode();

    public function getShipmentStatuses();

    public function getCustomerVerificationMethods();

    public function getTownIDs();
}
