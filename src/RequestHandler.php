<?php

namespace Shoppi;
require './vendor/autoload.php';
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use \Shoppi\model\TokenParams;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Handle all the request to the server
 *
 * @author bamtak
 */
class RequestHandler {

    private $base_uri = "https://zuul-service-dev.shoppi.ng/",
            $prefix = 'delivery_cloud/api/v2/',
            $client = NULL,
            $tokenRequester = NULL,
            $token = NULL;

    public function __construct($client_id, $client_secret) {
        $this->tokenRequester = new TokenParams($client_id, $client_secret);
        $this->client = new Client(['base_uri' => $this->base_uri]);
    }

    public function sendGetRequest($endpoint) {
        $this->refreshToken();
        try {
            $response = $this->client->request('GET', $this->prefix.$endpoint, 
                    ['headers' => ['Authorization' => 'Bearer' . $this->token->access_token]]);
            return json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            return $this->errorHandler($e);
        }
    }
    

    public function sendPostRequest($endpoint, $body) {
        $this->refreshToken();
        try {
            $response = $this->client->request('POST', $this->prefix.$endpoint, ['headers' =>
                ['Authorization' => 'Bearer' . $this->token->access_token], 'json' => $body]);
            return json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            return $this->errorHandler($e);
        }
    }

    private function getAccessToken() {
         try {
            $response = $this->client->request('POST', 'oauth/token', array(
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'service_id' => $this->tokenRequester->getService_id(),
                    'client_id' => $this->tokenRequester->getClient_id(),
                    'client_secret' => $this->tokenRequester->getClient_secret(),
                    'grant_type' => $this->tokenRequester->getGrant_type()
                ]
            ));
            $this->token = json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            return $this->errorHandler($e);
        }
    }

    private function errorHandler($e) {
        return json_decode($e->getResponse()->getBody(true)->getContents());
    }
    
    
    // Handle expire token
    private function refreshToken() {
        if($this->token == NULL){
            $this->getAccessToken();
            return;
        }
        else {
            $expires_in = $this->token->expires_in;
            //If token has expires get new one
            if ($expires_in) {
                $this->getAccessToken();
            }
        }
    }

}
