<?php
namespace Shoppi\model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Address Class holds address information
 * which include name, city, state, country, town_id
 * @author bamtak
 */
class Address {

    /**  @var string name */
    public $name;
    
    /**  @var string city */
    public $city;
    
    /**  @var string state */
    public $state;
    
    /**  @var string country */
    public $country;
    
    /**  @var string town_id */
    public $town_id;

    /*
     * 
     */
    public function __construct($name, $city, $state, $country, $town_id) {
        $this->name = $name;
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->town_id = $town_id;
    }
}
