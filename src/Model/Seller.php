<?php
namespace Shoppi\model;
use Shoppi\model\Address;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class holds Seller information
 *
 * @author bamtak
 */
class Seller {

    /**  @var string id */
    public $id;
    
    /**  @var string name */
    public $name;
    
    /**  @var string phone_number */
    public $phone_number;
    
    /**  @var string email */
    public $email;
    
    /**  @var address address */
    public $address_object;
    
    public function __construct($id, $name, $phone_number, $email, Address $address) {
        $this->id = $id;
        $this->name = $name;
        $this->phone_number = $phone_number;
        $this->email = $email;
        $this->address_object = $address;
    }
}
