<?php
namespace Shoppi\model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class developer parameters needed for token request
 *
 * @author bamtak
 */
class TokenParams {
    /** @var string client_id*/
    private $client_id;

    /** @var string client_secret*/
    private $client_secret;
    
    /** @var string  service_id*/
    private $service_id;

    /** @var string  grant_type*/
    private $grant_type;
    
    public function __construct($client_id, $client_secret) {
        $this->client_id = $client_id;
        $this->client_secret = $client_secret;
        $this->service_id = "delivery_cloud";
        $this->grant_type = "client_credentials";
    }
    
    public function getClient_id() {
        return $this->client_id;
    }

    public function getClient_secret() {
        return $this->client_secret;
    }

    public function getService_id() {
        return $this->service_id;
    }

    public function getGrant_type() {
        return $this->grant_type;
    }

    public function setClient_id($client_id) {
        $this->client_id = $client_id;
    }

    public function setClient_secret($client_secret) {
        $this->client_secret = $client_secret;
    }

    public function setService_id($service_id) {
        $this->service_id = $service_id;
    }

    public function setGrant_type($grant_type) {
        $this->grant_type = $grant_type;
    }

}
