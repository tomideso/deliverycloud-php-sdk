<?php
namespace Shoppi\model;
use Shoppi\model\Address;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * This class holds Customer information
 *
 * @author bamtak
 */
class Customer {

    /**  @var string unique_id */
    public $customer_id;
    
    /**  @var string first name */
    public $first_name;
    
    /**  @var string last name */
    public $last_name;
    
    /**  @var string phone number */
    public $phone_number;
    
    /**  @var string email */
    public $email;
    
    /**  @var address address of the customer */
    public $address;

    public function __construct($customer_id, $first_name, $last_name, $phone_number, $email, Address $address) {
        $this->customer_id = $customer_id;
        $this->first_name = $first_name;
        $this->last_name = $last_name;
        $this->phone_number = $phone_number;
        $this->email = $email;
        $this->address = $address;
    }
}
